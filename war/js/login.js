captureData = function(event) {
    var data = $('form[name="form-login"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://active-tangent-159416.appspot.com/rest/login/v2",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                alert("Got token with id: " + response.tokenID);
                // Store token id for later use in localStorage
                localStorage.setItem('username', response.username);
                localStorage.setItem('tokenID', response.tokenID);
                window.location.href = "http://active-tangent-159416.appspot.com/index.html";
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="form-login"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}
